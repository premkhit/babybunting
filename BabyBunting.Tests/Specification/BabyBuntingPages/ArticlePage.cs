﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBuntingPages
{
    [Binding]
    public static class ArticlePage
    {
        public static By ALCOMP_HeaderBanner() { return By.CssSelector("div.component.cxa-header-banner"); }
        public static By ALCOMP_SelectedArticle() { return By.CssSelector("div.component.cxa-related-articles"); }
        public static By ALCOMP_CTA() { return By.CssSelector("div.component.cxa-shop-prams"); }
        public static By ALCOMP_ListedArticle() { return By.CssSelector("div.component.cxa-article-list"); }
        public static By ArticlePage_HeroBanner() { return By.CssSelector(".component-content.field-image"); }
        public static By ArticlePage_Breadcrumb() { return By.CssSelector("div.component.breadcrumb.navigation-title.initialized"); }
        public static By ArticlePage_Summary() { return By.CssSelector("div.description-content.field-summary"); }
        public static By ArticlePage_RichText() { return By.CssSelector("div.content-article__container.container >div.component.image.file-type-icon-media-link"); }
        public static By ArticlePage_OneImage() { return By.XPath("//div[@class = 'component-content']//a[@title='Image 1']"); }
        public static By ArticlePage_AddThis() { return By.CssSelector("div.addthis_inline_share_toolbox"); }
        
    }
}
