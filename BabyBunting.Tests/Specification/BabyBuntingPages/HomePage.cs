﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BabyBunting.Tests.Specification.BabyBuntingPages
{

    public static class HomePage
    {
        public static By PopUpClose() { return By.XPath("//button[@type='button'] [@class='close']"); }
        public static By COMPONENT_MiniTile() { return By.XPath("//div[@class='component cxa-minitiles-component']"); }
        public static By COMPONENT_Slider() { return By.XPath("//div[@class='container content']//div[@class='category-products']"); }
        public static By COMPONENT_CallOutDefault() { return By.XPath("//div[@class='component cxa-imagetext-component col-xs-12']"); }
        public static By COMPONENT_CallOutTiles() { return By.XPath("//div[@class='row']//div[@class='container-fluid']/div/div[3]"); }
        public static By COMPONENT_CategoryTab() { return By.XPath("//div[@class='component cxa-categorytabs-component']"); }
        public static By COMPONENT_BrandCarousel() { return By.XPath("//div[@class='component cxa-brand-carousel']"); }
        public static By COMPONENT_CTACarousel() { return By.XPath("//ul[@class='slides owl-carousel owl-theme owl-loaded owl-drag']"); }
        public static By HEADER_CAT(string category) { return By.XPath("//nav[@class='product-categories-menu']//a[@href='" + category + "']"); }
        public static By Mobile_CATNav(string category) { return By.XPath("//div[@class='component navigation cxa-menu-component mobile-nav__sub-nav generated-nav active']//li/div[@class='menu-link field-navigationtitle']//a[@href='" + category + "']"); }
        public static By LABEL_CATEGORY_NAME() { return By.CssSelector("h2.heading"); }
        public static By HomePage_MiniCategoryTileList() { return By.XPath("//div[@class='component cxa-minitiles-component']//ul//a"); }
        public static By HomePage_ProductSliderList() { return By.XPath("//div[@class='category-products']//div[@class='product-list']//li//a"); }
        public static By HomePage_DefaultCallOutTile() { return By.XPath("//ul[@class='category-list owl-carousel owl-theme owl-loaded owl-drag']//li"); }
        public static By HomePage_CallOutTile() { return By.XPath("//ul[@class='category-list owl-carousel owl-theme owl-loaded owl-drag']//li"); }
        public static By HomePage_Tab() { return By.XPath("//ul[@class='tabs initialized owl-carousel owl-theme']//li"); }
        public static By HomePage_TabProductTile() { return By.XPath("//div[@class='category-tabs__panels']//div[@class='tab-content active']//div//a"); }
        public static By Menu_Mobile() { return By.CssSelector(".mobile-navigation"); }

    }
        public static class ProductListingPage {
            public static By BTN_LoadMore() { return By.CssSelector(".load-more>button"); }
            public static By CategoriesSubNav() { return By.CssSelector("div.component.cxa-subcategorynavigation-component"); }
        }

    }   
