﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BabyBunting.Tests.Specification.BabyBuntingPages
{
   public static class YourCartPage
    {
        public static By Increase_ShoppingCart() { return By.CssSelector("button.increase"); }
        public static By Decrease_ShoppingCart() { return By.CssSelector("button.decrease"); }
        public static By RemoveItem_ShoppingCart() { return By.XPath("//span[@data-bind='click: $parents[1].removeItem']"); }
        public static By Empty_ShoppingCart() { return By.CssSelector("div.component.cxa-categorytiles-component.cxa-categorytiles-component--three-column"); }
        public static By INPUT_PromoCode() { return By.CssSelector(".promo-code-input"); }
        public static By BTN_APPLY() { return By.CssSelector("button.add-promo-code-button"); }
        public static By BTN_PromoConfirm() { return By.XPath("//button[contains(.,'Promo')]"); }
        public static By Variant_Color() { return By.XPath("//div[@class='color-information']"); }
        public static By Variant_Size() { return By.XPath("//div[@class='size-information']"); }
        public static By Badge_Store() { return By.XPath("//div[@class='shoppingcart-location-badge']"); }
        public static By TXT_PromotionDiscount() { return By.XPath("//span[@class='summary-text'] [@title='Total discount']"); }

    }
}
