﻿using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBuntingPages
{
    [Binding]
    public static class AccountPageBeforeLogin
    {
        public static By BTN_CreateAnAccount() { return By.CssSelector(""); }
        public static By BTN_SignIN() { return By.CssSelector("button#SignInButton"); }
        public static By Create_ACCOUNT_INPUT_FirstName() { return By.CssSelector(""); }
        public static By Create_ACCOUNT_INPUT_LastName() { return By.CssSelector(""); }
        public static By Create_ACCOUNT_INPUT_EmailAddress() { return By.CssSelector(""); }
        public static By Create_ACCOUNT_INPUT_SetYourPassword() { return By.CssSelector(""); }
        public static By BTN_CreateGoogleACCOUNT() { return By.XPath("//li[@class='tab-panel__list-item active']//span[@class='text--button']/span[contains(.,'Sign in with Google')]"); }
        public static By SignIn_INPUT_RegisteredEmail() { return By.CssSelector("input#loginUsername"); }
        public static By SignIn_INPUT_RegisteredPassword() { return By.CssSelector("input#loginPassword"); }
        public static By Google_ACCOUNT_Emailaddress() { return By.XPath("//div[@class='Xb9hP']//input[@type='email']"); }
        //XPath("//div//input[@type='email']"); }
        public static By BTN_GoogleAccountNext() { return By.XPath("//content[@class='CwaK9']//span[contains(.,'Next')]"); }
        public static By Google_ACCOUNT_Password() { return By.XPath("//div[@class='Xb9hP']//input[@type='password']"); }
        public static By Google_ACCOUNT_BTN_SignIn() { return By.CssSelector(""); }
        public static By BTN_SignIn() { return By.CssSelector(""); }
    }
    public static class AccountPageLoggedIn
    {
        public static By Hover_Account() { return By.XPath("//div[@class='component container']//div[@class='topbarlinks-wrapper']"); }
        public static By BTN_Account() { return By.XPath("//div[@class='topbarlinks-wrapper']//a[@title='Account']"); }
    }
}