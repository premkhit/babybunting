﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BabyBunting.Tests.Specification.BabyBuntingPages
{
    public static class AccountPage
    {
        public static By DIV_EditProfile() { return By.CssSelector("div.component.cxa-profileeditor-component"); }
        public static By DIV_EditChangePassword() { return By.CssSelector("div.component.cxa-changepassword-component"); }
        public static By Link_AccountMenu(String LinkName) { return By.XPath("//div[@class='component navigation navigation-title col-xs-12 initialized']//a[@title='"+LinkName+"']"); }
        public static By DIV_AddressesAndDelivery() { return By.XPath("//h3[@class='field-title']"); }
        public static By DIV_OrderHistory() { return By.XPath("//div[@class='field-link']//a[contains(.,'Returns &')]"); }
        

    }
}
