﻿    using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBuntingPages
{
    public static class RegisterPage

    {
        public static By INPUT_FirstName() { return By.CssSelector("input#FirstName"); }
        public static By INPUT_LastName() { return By.CssSelector("input#LastName"); }
        public static By INPUT_EmailAddress() { return By.CssSelector("input#UserName"); }
        public static By INPUT_Password() { return By.CssSelector("input#Password"); }
        public static By INPUT_ConfirmPassword() { return By.CssSelector("input#ConfirmPassword"); }
        public static By BTN_SignUP() { return By.CssSelector("button#registerButton"); }
    }
}
