﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBuntingPages
{
    [Binding]
    public class StorePage
    {
        public static By DIV_FindStoreComponent() { return By.CssSelector(".component.storenavigation-component.store-finder-component.store-sidebar"); }
        public static By Accordion_Stores() { return By.CssSelector(".menu-lists"); }
        public static By Nearest_Stores() { return By.CssSelector(".row.store-lists"); }
        public static By Nearest_Stores_Tile() { return By.CssSelector(".row.store-lists>div>a"); }
        public static By StoreName(){ return By.XPath("//h3[@data-bind]"); }
        
    }
}
