﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBuntingPages
{
    public static class CartPage
    {
        public static By Hover_MiniCart() { return By.CssSelector(".top-text"); }
        public static By BTN_Mini_YourCart() { return By.CssSelector("a.view-your-cart"); }
        public static By BTN_Checkout() { return By.CssSelector(".component.link.primary-button>div>div>a"); }
        public static By GUEST_Address_CHECKOUT() { return By.XPath("//div[@class='component custom--checkout checkout-address']"); }
        public static By SubtotalPrice() { return By.XPath("//span[@class='summary-amount'] [@data-bind='text: subTotal']"); }
        public static By TermandoShippingFee() { return By.XPath("//span[@class='summary-amount'] [@data-bind='text: shippingTotal']"); }
        public static By TotalPrice() { return By.CssSelector("h4.total"); }
        public static By BTN_ClosePrivacyBar() { return By.XPath("//div[@class='privacy-warning acceptonclose']//div[@class='close']/a"); }
        public static By BTN_GuestCheckout() { return By.XPath("//div[@class='component link']//a[@href='/checkout/delivery']"); }
        
    }
}
