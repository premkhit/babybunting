﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBuntingPages
{
    [Binding]
    public static class GuestCheckoutOrderPlace
    {
        public static By LINK_ManualAdress() { return By.XPath("//a[@class='link-manual-address']"); }
        public static By INPUT_FirstName() { return By.CssSelector(".shipall-firstname>input"); }
        public static By INPUT_State() { return By.CssSelector(".shipall-state>input"); }
        public static By INPUT_LastName() { return By.CssSelector(".shipall-lastname>input"); }
        public static By INPUT_Address() { return By.CssSelector(".shipall-address>input"); }
        public static By INPUT_City() { return By.CssSelector(".shipall-city>input"); }
        public static By INPUT_Postcode() { return By.CssSelector(".shipall-zipcode>input"); }
        public static By INPUT_PhoneNumber() { return By.CssSelector(".shipall-phoneNumber>input"); }
        public static By INPUT_CCFirstName() { return By.CssSelector(".address-firstname>input"); }
        public static By INPUT_CCState() { return By.CssSelector(".address-state-name>input"); }
        public static By INPUT_CCLastName() { return By.CssSelector(".address-lastname>input"); }
        public static By INPUT_CCAddress() { return By.CssSelector(".address-street>input"); }
        public static By INPUT_CCCity() { return By.CssSelector(".address-city-name>input"); }
        public static By INPUT_CCPostcode() { return By.CssSelector(".address-city-zipcode>input"); }
        public static By INPUT_CCPhoneNumber() { return By.CssSelector(".address-phoneNumber>input"); }
        public static By BTN_NextGetShipping() { return By.CssSelector(".btn-delivery-next"); }
        public static By BTN_ShippingFeeSection() { return By.XPath("//h3[@data-bind='visible: cart().hasDeliveryShipment']"); }
        public static By Radio_Shipping() { return By.XPath("//div[@class='shipping-checkbox-item']//input[@type='radio']"); }
        public static By BTN_NextGetBilling() { return By.XPath("//button [@id='ToBillingButton']"); }
        public static By Radio_CreditCard() { return By.Id("div#component-content div.payment-radio-groups > div:nth-child(1)"); }
        public static By IFrame_BT() { return By.XPath("//iframe[contains(@src,'assets.braintreegateway.com/dropin')]"); }
        public static By INPUT_CCNumber() { return By.Id("credit-card-number"); }
        public static By INPUT_Email() { return By.Id("emailAddress"); }
        public static By INPUT_ExpiryDate() { return By.Id("expiration"); }
        public static By INPUT_CVV() { return By.Id("cvv"); }
        public static By BTN_ConfirmPayment() { return By.XPath("//button[@id='confirmPaymentDetailsButton']"); }
        public static By RDOBTN_ZipCode() { return By.Id("paymentOption1"); }
        public static By BTN_PlaceOrder() { return By.XPath("//button[@id='placeOrderButton']"); }
        public static By Txt_CheckoutPage() { return By.CssSelector("h1.page-header__title_text"); }
        public static By Txt_OrderConfirmation() { return By.XPath("//h1[@class='page-header__title_text'] [contains(.,'Thank you for your order!')]"); }
        
    }
}