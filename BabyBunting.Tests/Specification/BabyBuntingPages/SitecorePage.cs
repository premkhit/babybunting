﻿using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBuntingPages
{
    [Binding]
    public class SitecorePage
    {
        public static By DIV_SitecoreLogin() { return By.CssSelector("div.login-box"); }
        public static By INPUT_Loginfield() { return By.CssSelector("form#LoginForm"); }
        public static By INPUT_Username() { return By.CssSelector("input#UserName.form-control"); }
        public static By INPUT_Password() { return By.CssSelector("input#Password.form-control"); }
        public static By BTN_LogOn() { return By.CssSelector("input#LogInBtn"); }



        public class ContentEditor
        {
            public static By DIV_ApplicationMenu() { return By.XPath("//a[@class='sc-launchpad-item'][@title='Content Editor']"); }
        }
            public class Login
            {
                public static By BTN_ContentEditor() { return By.XPath("//a[@class='sc-launchpad-item'][@title='Content Editor']"); }
            }
        }
    }

