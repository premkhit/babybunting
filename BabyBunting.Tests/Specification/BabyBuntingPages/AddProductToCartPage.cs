﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBuntingPages
{
    public static class AddProductToCartPage 
    {
        public static By ProductCategoryNavMenu() { return By.XPath("//li[@class='category-item']//a[@href ='/shop/Car-Seats=babybunting-438']"); }
        public static By LINK_ProductTiles() { return By.CssSelector(".product-listing-content"); }
        public static By LINK_ProductTiles1() { return By.XPath("//div[@class='component product-summary product-item-container']//div[@class='photo']/a"); }
        public static By LABEL_ProductTitle() { return By.CssSelector("h1#displayName"); }
        public static By LABEL_StrikethroughPrice() { return By.CssSelector("div.price-now-before"); }
        public static By LABEL_DiscountedPrice() { return By.XPath("//div[@class='price-now-before']//span[@data-bind='text: priceNow']"); }
        public static By BTN_AddToCart() { return By.XPath("//button[@type ='submit'] [@class='add-to-cart-btn']"); }
        public static By LABEL_ProductPrice() { return By.XPath("//div[@class='price-only']//span[@data-bind]"); }
        public static By MiniCart_Count() { return By.CssSelector(".cart-items-count"); }
        public static By ProductName() { return By.Id("displayName"); }
        public static By Tabs_Heading() { return By.XPath("//strong[@data-bind = 'text: deliveryLocation().suburb()']"); }
    }
    public static class CCAddProductToCartPage {
        public static By TAB_CC() { return By.XPath("//li[@data-fulfillment = 'pickup']"); }
        public static By Radio_CC() { return By.CssSelector("input#item_101"); }

    }

}
