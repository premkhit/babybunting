﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BabyBunting.Tests.Specification.BabyBuntingPages
{
    public static class SearchPage
    {
        public static By INPUT_SearchBox() { return By.CssSelector("input.search-textbox"); }
        public static By PopUp_Close() { return By.CssSelector("button.close"); }
        public static By SearchIcon() { return By.CssSelector("button.search-button"); }
        public static By SearchHeader() { return By.CssSelector("h1.page-header__title_text"); }
        public static By MobileSearchIcon() { return By.CssSelector(".toggle-search-bar"); }
    }
}
