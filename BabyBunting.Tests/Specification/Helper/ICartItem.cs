﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BunningsPU.Specification.Helper
{
    public interface ICartItem
    {
        string name { get; set; }
        int productNumber { get; set; }
        int quantity { get; set; }
        decimal unitPrice { get; set; }
        decimal miniCartSubtotal { get;  }
        decimal miniCartTotal { get; set; }
        decimal subTotal { get; }
        string deliveryDetails { get; set; }
    }
}
