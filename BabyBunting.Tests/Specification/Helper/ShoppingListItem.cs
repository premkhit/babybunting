﻿using BunningsPU.Specification.Helper;

namespace BunningsPU.Specification
{
    class ShoppingListItem : ICartItem
    {
        public string name { get; set; }
        public int productNumber { get; set; }
        public int quantity { get; set; }
        public decimal unitPrice { get; set; }
        public decimal miniCartSubtotal { get; set; }
        public decimal termandoFee { get; set; }
        public decimal miniCartTotal { get; set; }
        public decimal subTotal
        {
            get
            {
                return quantity * unitPrice;
            }
        }
        public string deliveryDetails
        {
            get
            {
                return deliveryDetails;
            }
            set
            {
                value = null;
            }
        }
    }
}
