﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BunningsPU.Specification.Helper
{
    public class Cart
    {
        private List<ICartItem> cart;

        private decimal _deliveryFee = -1m;
        public decimal deliveryFee
        {
            get
            {
                if (_deliveryFee < 0m)
                {
                    //_deliveryFee = 0m;
                    //var deliveryDetails = new List<string>();
                    //foreach (var item in cart)
                    //{
                    //    if (!deliveryDetails.Contains(item.deliveryDetails))
                    //    {
                    //        var deliveryType = item.deliveryDetails.Split(',').Last().Trim();
                    //        if (deliveryType.Contains("Regular"))
                    //            _deliveryFee = _deliveryFee + (("" + Context.Website).Contains("AU") ? AU_RegularDeliveryFee : NZ_RegularDeliveryFee);
                    //        else if (deliveryType.Contains("Express"))
                    //            _deliveryFee = _deliveryFee + AU_ExpressDeliveryFee;
                    //        else
                    //            _deliveryFee = 0m;

                    //        deliveryDetails.Add(item.deliveryDetails);
                    //    }
                    //}
                }
                return _deliveryFee;
            }
        }

        public Cart()
        {
            cart = new List<ICartItem>();
        }

        public List<ICartItem> Items()
        {
            return cart;
        }

        public void AddItem(ICartItem item)
        {
            cart.Add(item);
        }

        public ICartItem GetItem(string itemName)
        {
            foreach (var item in cart)
            {
                if (item.name.ToLower().Equals(itemName.ToLower()))
                {
                    return item;
                }
            }

            return null;
        }

        public ICartItem GetItem(int itemNumber)
        {
            return cart.First(i => i.productNumber == itemNumber);
        }

        public ICartItem GetItemByIndex(int i)
        {
            return cart[i];
        }

        public int NumOfItem()
        {
            var total = 0;
            foreach (var item in cart)
            {
                total = total + item.quantity;
            }
            return total;
        }

        public int RowItem()
        {
            return cart.Count;
        }

        public decimal TotalBeforeDelivery()
        {
            var total = 0m;
            foreach (var item in cart)
            {
                total = total + (item.quantity * item.unitPrice);
            }
            return total;
        }

        public decimal TotalAfterDelivery()
        {
            return TotalBeforeDelivery() + deliveryFee;
        }

        public override string ToString()
        {
            var text = "";
            int i = 1;
            foreach (var item in cart)
            {
                text = text + "Item " + i + " is:\n";
                text = text + item.name + "\n";
                text = text + item.productNumber + "\n";
                text = text + "Quantity: " + item.quantity + "\n";
                text = text + "Unit price: " + item.unitPrice + "\n";
                text = text + "Subtotal price: " + item.subTotal + "\n";

                i++;
            }
            return text;
        }
    }
}
