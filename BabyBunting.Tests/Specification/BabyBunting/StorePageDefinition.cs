﻿using BabyBunting.Tests.Specification.BabyBuntingPages;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBunting
{
    [Binding]
    public class StorePageDefinition : BaseSteps
    {
        private HttpWebRequest task;

        [StepDefinition(@"I can see the following options in Store page")]
        public void ThenICanSeeTheFollowingOptions(Table table)
        {
            foreach (var row in table.Rows)
            {
                switch (row["options"].ToLower())
                {
                    case "find component store":
                        Assert.AreEqual(1, Browser.FindElements(StorePage.DIV_FindStoreComponent()).Count);
                        break;
                    case "stores accordion":
                        Assert.AreEqual(1, Browser.FindElements(StorePage.Accordion_Stores()).Count);
                        Assert.IsTrue(Browser.FindElement(StorePage.Accordion_Stores()).Displayed);
                        break;
                    case "nearest stores":
                        Assert.AreEqual(1, Browser.FindElements(StorePage.Nearest_Stores()).Count);
                        break;

                    default:
                        throw new Exception("ERROR: Unrecognized element of " + row["options"]);
                }
            }
        }
        [StepDefinition(@"I Randomly Select One Of Store From Nearest Stores Tiles")]
        public void WhenIRandomlySelectOneOfStoresFromNearestStoresTiles()
        {
            // NOTE: Current implementation will not allow us to randomly select store
            Wait.Until(ExpectedConditions.ElementExists(StorePage.Nearest_Stores_Tile()));
            var Stores = Browser.FindElements(StorePage.Nearest_Stores_Tile()).ToList();
            if (Stores.Count == 0)
                throw new Exception("ERROR: Cannot select any store as there are no stores in the store listing section...");

            var store = Stores[(new Random()).Next(0, Stores.Count())];
            store.Click();
            

        }
        [StepDefinition(@"I will be in Store detail page")]
        public void ThenIWillBeInStoreDetailPage()
        {
            Wait.Until(ExpectedConditions.ElementIsVisible(StorePage.StoreName()));
            Assert.IsTrue(Browser.FindElement(StorePage.StoreName()).Displayed);
        }

    }
}
