﻿using BabyBunting.Tests.Specification.BabyBuntingPages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBunting
{
    [Binding]
    public class ArticleStepDefinition : BaseSteps
    {
        [StepDefinition(@"i see following component")]
        public void ThenISeeTheComponent(Table section)
        {
            foreach (var row in section.Rows)
            {
                switch (row["section"].ToLower())
                {
                    case "header banner":
                        Assert.IsTrue(Browser.FindElement(ArticlePage.ALCOMP_HeaderBanner()).Displayed);
                        break;
                    case "selected article":
                        Assert.IsTrue(Browser.FindElement(ArticlePage.ALCOMP_SelectedArticle()).Displayed);
                        break;
                    case "cta banner":
                        Assert.IsTrue(Browser.FindElement(ArticlePage.ALCOMP_CTA()).Displayed);
                        break;
                    case "article hero":
                        Assert.IsTrue(Browser.FindElement(ArticlePage.ArticlePage_HeroBanner()).Displayed);
                        break;
                    case "article breadcrumb":
                        Assert.IsTrue(Browser.FindElement(ArticlePage.ArticlePage_Breadcrumb()).Displayed);
                        break;
                    case "listed article":
                        Assert.IsTrue(Browser.FindElement(ArticlePage.ALCOMP_ListedArticle()).Displayed);
                        break;
                    case "article summary":
                        Assert.IsTrue(Browser.FindElement(ArticlePage.ArticlePage_Summary()).Displayed);
                        break;
                    case "article rich text":
                        Assert.IsTrue(Browser.FindElement(ArticlePage.ArticlePage_RichText()).Displayed);
                        break;
                    case "one image":
                        Assert.IsTrue(Browser.FindElement(ArticlePage.ArticlePage_OneImage()).Displayed);
                        break;
                    case "share this":
                        Assert.IsTrue(Browser.FindElement(ArticlePage.ArticlePage_AddThis()).Displayed);
                        break;
                    default:
                        throw new Exception("ERROR: Unrecognized field name of " + row["Tabs"]);
                }
            }
        }
    }
}

