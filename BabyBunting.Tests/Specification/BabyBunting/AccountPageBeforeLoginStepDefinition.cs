﻿using BabyBunting.Tests.Specification.BabyBuntingPages;
using BabyBunting.Tests.Specification.Helper;
using NUnit.Framework;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Net;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification
{
    [Binding]
    public class AccountPageBeforeLoginStepDefinition : BaseSteps 
    {
        private HttpWebRequest task;

        [StepDefinition(@"I can see the following options")]
        public void ThenICanSeeTheFollowingOptions(Table table)
        {
            foreach (var row in table.Rows)
            {
                switch (row["options"].ToLower())
                {
                    case "create an account":
                        Assert.AreEqual(1, Browser.FindElements(AccountPageBeforeLogin.BTN_CreateAnAccount()).Count);
                        break;

                    case "sign in":
                        Assert.AreEqual(1, Browser.FindElements(AccountPageBeforeLogin.BTN_SignIN()).Count);
                        break;

                    default:
                        throw new Exception("ERROR: Unrecognized element of " + row["options"]);
                }
            }
        }
        [StepDefinition(@"I will see form (.*)")]
        [StepDefinition(@"I click on (.*) button")]
        [StepDefinition(@"I click on (.*) checkbox")]
        public void WhenIClickOnButton(String buttonName)
        {
            switch (buttonName.ToLower())
            {
                case "create an account":
                    Browser.FindElement(AccountPageBeforeLogin.BTN_CreateAnAccount()).Click();
                    break;
                case "sign in":
                    Browser.FindElement(AccountPageBeforeLogin.BTN_SignIN()).Click();
                    Browser.WaitElementToDisappear(AccountPageBeforeLogin.BTN_SignIN());
                    break;
                case "create account form":
                    Assert.IsTrue(Browser.FindElement(AccountPageBeforeLogin.BTN_CreateAnAccount()).Displayed);
                    break;
                case "create account":
                    Browser.FindElement(AccountPageBeforeLogin.BTN_CreateAnAccount()).Click();
                    break;
                case "sign in form":
                    Assert.IsTrue(Browser.FindElement(AccountPageBeforeLogin.BTN_CreateAnAccount()).Displayed);
                    break;
                case "registered user sign in":
                    Browser.FindElement(AccountPageBeforeLogin.BTN_CreateAnAccount()).Click();
                    break;
                case "receive email":
                    Browser.FindElement(AccountPageBeforeLogin.BTN_CreateAnAccount()).Click();
                    break;
                case "place order":
                    if (Browser.Url.Contains("https://cd-dev.babybunting.com.au"))
                    {
                        Browser.FindElement(GuestCheckoutOrderPlace.BTN_PlaceOrder()).Click();
                        Browser.WaitForUrlToLoad("/checkout/orderconfirmation?");
                        Console.WriteLine("The steps to click on Place order executed as its dev QA environment");
                    }

                    else
                    {
                        Assert.IsTrue(Browser.FindElement(GuestCheckoutOrderPlace.BTN_PlaceOrder()).Displayed);
                        Console.WriteLine("The Steps not to click on Place Order executed as its prod environment");
                    }                              
                    break;
                case "google account":
                    if (Browser.FindElement(CartPage.BTN_ClosePrivacyBar()).Displayed)
                    Browser.FindElement(CartPage.BTN_ClosePrivacyBar()).Click();
                    Browser.FindElement(AccountPageBeforeLogin.BTN_CreateGoogleACCOUNT()).Click();
                    break;
                case "google next":
                    Browser.FindElement(AccountPageBeforeLogin.BTN_GoogleAccountNext()).Click();
                    break;
                default:
                    Browser.Navigate().GoToUrl(BaseUrl + buttonName);
                    task = (HttpWebRequest)WebRequest.Create(BaseUrl + buttonName);
                    break;
            }
        }
        [StepDefinition(@"I enter the following in sign in page")]
        public void WhenIEnterTheFollowingInAccountPage(Table table)
        {
            var uri = new Uri(Browser.Url);

            if (uri.AbsoluteUri.Contains("#login"))
            {
                var randomString = RandomString.Random(5);
                var currDateTime = DateTime.Now.ToString("MMddyyHmmss");
                foreach (var row in table.Rows)
                {
                    var value = row["Value"].Replace("{RANDOM STRING}", randomString).Replace("{CURR DATE TIME}", currDateTime);
                    switch (row["Field name"].ToLower())
                    {
                        case "first name":
                            Browser.ClearThenSendKeys(AccountPageBeforeLogin.Create_ACCOUNT_INPUT_FirstName(), value);
                            break;
                        case "last name":
                            Browser.ClearThenSendKeys(AccountPageBeforeLogin.Create_ACCOUNT_INPUT_LastName(), value);
                            break;
                        case "email":
                            Browser.ClearThenSendKeys(AccountPageBeforeLogin.Create_ACCOUNT_INPUT_EmailAddress(), value);
                            break;
                        case "password":
                            Browser.ClearThenSendKeys(AccountPageBeforeLogin.Create_ACCOUNT_INPUT_SetYourPassword(), value);
                            break;
                        case "registered email":
                            Browser.ClearThenSendKeys(AccountPageBeforeLogin.SignIn_INPUT_RegisteredEmail(), value);
                            break;
                        case "registered password":
                            Browser.ClearThenSendKeys(AccountPageBeforeLogin.SignIn_INPUT_RegisteredPassword(), value);
                            break;
                        case "google email address":
                            Browser.ClearThenSendKeys(AccountPageBeforeLogin.Google_ACCOUNT_Emailaddress(), value);
                            break;
                        case "google password":
                            Browser.ClearThenSendKeys(AccountPageBeforeLogin.Google_ACCOUNT_Password(), value);
                            Browser.FindElement(AccountPageBeforeLogin.BTN_GoogleAccountNext()).Click();
                            break;
                         default:
                            throw new Exception("ERROR: Unrecognized field name of " + row["Field name"]);
                    }
                }

            }
        }
        [StepDefinition(@"I enter the following in google password page")]
        [StepDefinition(@"I enter the following in google email address page")]
        public void WhenIEnterTheFollowingInGoogleEmailAddressPage(Table table)
        {
            var uri = new Uri(Browser.Url);

            if (uri.AbsoluteUri.Contains("/sign"))
            {
                var randomString = RandomString.Random(5);
                var currDateTime = DateTime.Now.ToString("MMddyyHmmss");
                foreach (var row in table.Rows)
                {
                    var value = row["Value"].Replace("{RANDOM STRING}", randomString).Replace("{CURR DATE TIME}", currDateTime);
                    switch (row["Field name"].ToLower())
                    { 
                     
                        case "google email address":
                            Browser.ClearThenSendKeys(AccountPageBeforeLogin.Google_ACCOUNT_Emailaddress(), value);
                            break;
                        case "google password":
                            Browser.ClearThenSendKeys(AccountPageBeforeLogin.Google_ACCOUNT_Password(), value);
                           // Browser.FindElement(AccountPageBeforeLogin.BTN_GoogleAccountNext()).Click();
                            break;
                        default:
                            throw new Exception("ERROR: Unrecognized field name of " + row["Field name"]);
                    }
                }

            }
        }
        [StepDefinition(@"I can see user is logged in")]
        public void ThenICanSeeUserIsLoggedIn()
        {
            Wait.Until(ExpectedConditions.ElementIsVisible(AccountPageLoggedIn.Hover_Account()));
            Actions tooltip = new Actions(Browser);
            tooltip.MoveToElement(Browser.FindElement(AccountPageLoggedIn.Hover_Account())).Build().Perform();
            Wait.Until(ExpectedConditions.ElementIsVisible(AccountPageLoggedIn.BTN_Account()));
            Assert.IsTrue(Browser.FindElement(AccountPageLoggedIn.BTN_Account()).Displayed);
        }

    }

}
