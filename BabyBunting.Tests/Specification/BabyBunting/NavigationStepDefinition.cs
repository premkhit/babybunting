﻿using BabyBunting.Tests.Specification.BabyBuntingPages;
using BabyBunting.Tests.Specification.Helper;
using OpenQA.Selenium;
using System.Net;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBunting
{
    [Binding]
    public class NavigationStepDefinition : BaseSteps
    {
        private HttpWebRequest task; //For Calling the page
        private readonly HttpWebResponse taskresponse = null; //Response returned
        [Then(@"I will be redirected to (.*)")]
        [StepDefinition(@"I (?:navigate to|am at) (.*)")]
        public void WhenINavigate(string pageNameOrUrl)
        {
            switch (pageNameOrUrl.ToLower())
            {
                case "bbhomepage":
                    Browser.Navigate().GoToUrl(BaseUrl);
                    if (Browser.FindElement(CartPage.BTN_ClosePrivacyBar()).Displayed)
                    {
                        IWebElement element1 = Browser.FindElement(CartPage.BTN_ClosePrivacyBar());
                        ((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].click();", element1);
                    }
                    break;
                case "productvarintdetailpage":
                    Browser.Navigate().GoToUrl(BaseUrl + Config.GetRequiredValue("BBCD.VariantProductUrl"));
                    break;
                    
                case "mobilehomepage":
                    Browser.Navigate().GoToUrl(BaseUrl);
                    if (Browser.FindElement(CartPage.BTN_ClosePrivacyBar()).Displayed)
                        Browser.FindElement(CartPage.BTN_ClosePrivacyBar()).Click();
                    break;
                case "babytalk page":
                    Browser.Navigate().GoToUrl(BaseUrl + "/babytalk");
                    break;
                case "registration page":
                    Browser.Navigate().GoToUrl(BaseUrl + "/sign-in#register");
                    if (Browser.FindElement(CartPage.BTN_ClosePrivacyBar()).Displayed)
                    {
                        IWebElement element1 = Browser.FindElement(CartPage.BTN_ClosePrivacyBar());
                        ((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].click();", element1);
                    }
                    break;
                case "login page":
                    Browser.Navigate().GoToUrl(BaseUrl + "/sign-in#login");
                     if (Browser.FindElement(CartPage.BTN_ClosePrivacyBar()).Displayed)
                    {
                        IWebElement element1 = Browser.FindElement(CartPage.BTN_ClosePrivacyBar());
                        ((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].click();", element1);
                    }
                    //Browser.FindElement(CartPage.BTN_ClosePrivacyBar()).Click();
                    break;
                case "babytalk article":
                    Browser.Navigate().GoToUrl(BaseUrl + "/babytalk/pregnancy/a-guide-to-pregnancy-terminology");
                    break;
                case "productdetailpage":
                    Browser.Navigate().GoToUrl(BaseUrl + "/shop/wipes=babybunting-656/reynard-everyday-wipes-100-pack=105516");
                    break;
                case "sitecore":
                    Browser.Navigate().GoToUrl(BaseUrl + "/sitecore");
                    break;
                case "profile page":
                    Browser.Navigate().GoToUrl(BaseUrl + "/accountmanagement/editprofile");
                    break;
                case "store page":
                    Browser.Navigate().GoToUrl(BaseUrl + "/find-a-store?");
                    break;
                case "big and bulky pd page":
                    Browser.Navigate().GoToUrl(BaseUrl + "/shop/cots=babybunting-483/boori-alice-cot---almond=111377");
                    break;
                default:
                    Browser.Navigate().GoToUrl(BaseUrl + pageNameOrUrl);
                    task = (HttpWebRequest)WebRequest.Create(BaseUrl + pageNameOrUrl);
                    break;
            }
        }
    }
}
