﻿using BabyBunting.Tests.Specification.BabyBuntingPages;
using NUnit.Framework;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TechTalk.SpecFlow;
using BunningsPU.Specification.Helper;
using BunningsPU.Specification;
using BabyBunting.Tests.Specification.Helper;
using OpenQA.Selenium;

namespace BabyBunting.Tests.Specification.BabyBunting
{
    [Binding]
    public class AddProductToCartStepDefinition : BaseSteps
    {
        public static Cart shoppingList
        {
            get
            {
                if (!ScenarioContext.Current.ContainsKey("shopping list"))
                {
                    ScenarioContext.Current["shopping list"] = new Cart();
                }
                return (Cart)ScenarioContext.Current["shopping list"];
            }
        }

        public static List<string> SelectedProducts
        {
            get
            {
                if (!ScenarioContext.Current.ContainsKey("selected products"))
                {
                    ScenarioContext.Current["selected products"] = new List<string>();
                }
                return (List<string>)ScenarioContext.Current["selected products"];
            }
        }

        //[StepDefinition(@"I select category pram")]
        //public void GivenISelectCategoryPram()
        //{
        //    Browser.FindElement(AddProductToCartPage.ProductCategoryNavMenu()).Click();
        //}

        [StepDefinition(@"I will be in product listing page")]
        public void ThenIWillBeInProductListingPage()
        {
            Assert.IsTrue(Browser.FindElement(AddProductToCartPage.LINK_ProductTiles()).Displayed);
            Thread.Sleep(3000);
          Wait.Until(Browser=>Browser.ElementExists(AddProductToCartPage.LINK_ProductTiles()));
        }
        [StepDefinition(@"I select randomly one of the product in the product listing")]
        public void WhenISelectRandomlyOneProductInTheProductListing()
        {
            // NOTE: Current implementation will not allow us to randomly select product twice
           // Wait.Until(ExpectedConditions.ElementExists(AddProductToCartPage.LINK_ProductTiles()));
            var products = Browser.FindElements(AddProductToCartPage.LINK_ProductTiles1()).ToList();
            if (products.Count == 0)
                throw new Exception("ERROR: Cannot select any product as there are no product in the product listing section...");

            var product = products[(new Random()).Next(0, products.Count())];
            while (SelectedProducts.Contains(product.Text))
            {
                products.Remove(product);
                if (products.Count() == 0)
                    throw new Exception("ERROR: No more products to select as all products in this product listing page have been selected previously...");
                product = products[(new Random()).Next(0, products.Count())];
            }
            SelectedProducts.Add(product.Text);
            Console.WriteLine("INFO: Selecting product is " + product.Text);
            product.Click();
           // Thread.Sleep(3000);
            Wait.Until(Browser=>Browser.ElementExists(AddProductToCartPage.LABEL_ProductTitle()));

        }
        [StepDefinition(@"I will be in Product detail page")]
        public void ThenIWillBeInProductDetailPage()
        {
            Assert.IsTrue(Browser.FindElement(AddProductToCartPage.BTN_AddToCart()).Enabled);
            //Wait.Until(Browser => Browser.FindElement(AddProductToCartPage.Tabs_Heading()));
        }

        [StepDefinition(@"I add product to cart")]
        [StepDefinition(@"the product is added to cart")]
        public void WhenIAddProductToCart()
        {
            if (Browser.FindElement(CartPage.BTN_ClosePrivacyBar()).Displayed)
            {
                IWebElement element1 = Browser.FindElement(CartPage.BTN_ClosePrivacyBar());
                ((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].click();", element1);
            }
            Browser.FindElement(AddProductToCartPage.BTN_AddToCart()).Click();
            Wait.Until(Browser => Browser.FindElement(AddProductToCartPage.MiniCart_Count()).Text.Contains("1"));
            if (Browser.ElementExists(AddProductToCartPage.LABEL_StrikethroughPrice()))
            {
                Console.WriteLine("Check Strikethrough price is handled");
                var item = new ShoppingListItem
                {
                    name = Browser.Text(AddProductToCartPage.LABEL_ProductTitle()),
                    unitPrice = decimal.Parse(Browser.Text(AddProductToCartPage.LABEL_DiscountedPrice()).Replace("$", "")),
                    quantity = 1
                };
                shoppingList.AddItem(item);
                ScenarioContext["Products Price"] = decimal.Parse(Browser.Text(AddProductToCartPage.LABEL_DiscountedPrice()).Replace("$", ""));
                Console.WriteLine("INFO:Added item to cart with name " + item.name + item.unitPrice);
                WebDriverWait wait = new WebDriverWait(Browser, TimeSpan.FromSeconds(10));
                wait.IgnoreExceptionTypes(typeof(OpenQA.Selenium.StaleElementReferenceException)); // ignore stale exception issues
                wait.Until(d => Browser.FindElement(AddProductToCartPage.MiniCart_Count()).Text.Contains("1"));
                Console.WriteLine("INFO:Cart Count " + Browser.FindElement(AddProductToCartPage.MiniCart_Count()).Text);
                Assert.AreEqual(Browser.FindElement(AddProductToCartPage.MiniCart_Count()).Text, "1");
            }
            else
            {
                var item = new ShoppingListItem
                {
                    name = Browser.Text(AddProductToCartPage.LABEL_ProductTitle()),                  
                unitPrice = decimal.Parse(Browser.Text(AddProductToCartPage.LABEL_ProductPrice()).Replace("$", "")),
                    quantity = 1
                };
                shoppingList.AddItem(item);
                ScenarioContext["Products Price"] = decimal.Parse(Browser.Text(AddProductToCartPage.LABEL_ProductPrice()).Replace("$", ""));
                Console.WriteLine("INFO: Added item to cart with name " + item.name + "and Price is : " + item.unitPrice);
                Assert.AreEqual(Browser.FindElement(AddProductToCartPage.MiniCart_Count()).Text, "1");
            }
            
        }
        [StepDefinition(@"I can add C&C to cart page")]
        public void ThenICanAddCCToCartPage()
        {
            if (Browser.FindElement(CartPage.BTN_ClosePrivacyBar()).Displayed)
            {
                IWebElement element1 = Browser.FindElement(CartPage.BTN_ClosePrivacyBar());
                ((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].click();", element1);
            }
            IWebElement element = Browser.FindElement(CCAddProductToCartPage.TAB_CC());
            ((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Browser.FindElement(CCAddProductToCartPage.TAB_CC()).Click();
            Wait.Until(Browser => Browser.FindElement(CCAddProductToCartPage.Radio_CC()).Enabled);
            Browser.FindElement(CCAddProductToCartPage.Radio_CC()).Click();
           Wait.Until(Browser => Browser.FindElement(AddProductToCartPage.BTN_AddToCart()).Enabled);
            Browser.FindElement(AddProductToCartPage.BTN_AddToCart()).Click();
            Thread.Sleep(2000);
            if (Browser.ElementExists(AddProductToCartPage.LABEL_StrikethroughPrice()))
            {
                Console.WriteLine("Check Strikethrough price is handled");
                var item = new ShoppingListItem
                {
                    name = Browser.Text(AddProductToCartPage.LABEL_ProductTitle()),
                    unitPrice = decimal.Parse(Browser.Text(AddProductToCartPage.LABEL_DiscountedPrice()).Replace("$", "")),
                    quantity = 1
                };
                shoppingList.AddItem(item);
                ScenarioContext["Products Price"] = decimal.Parse(Browser.Text(AddProductToCartPage.LABEL_DiscountedPrice()).Replace("$", ""));
                Console.WriteLine("INFO:Added item to cart with name " + item.name + item.unitPrice);
                WebDriverWait wait = new WebDriverWait(Browser, TimeSpan.FromSeconds(10));
                wait.IgnoreExceptionTypes(typeof(OpenQA.Selenium.StaleElementReferenceException)); // ignore stale exception issues
                wait.Until(d => Browser.FindElement(AddProductToCartPage.MiniCart_Count()).Text.Contains("1"));
                Console.WriteLine("INFO:Cart Count " + Browser.FindElement(AddProductToCartPage.MiniCart_Count()).Text);
                Assert.AreEqual(Browser.FindElement(AddProductToCartPage.MiniCart_Count()).Text, "1");
            }
            else
            {
                var item = new ShoppingListItem
                {
                    name = Browser.Text(AddProductToCartPage.LABEL_ProductTitle()),
                    unitPrice = decimal.Parse(Browser.Text(AddProductToCartPage.LABEL_ProductPrice()).Replace("$", "")),
                    quantity = 1
                };
                shoppingList.AddItem(item);
                ScenarioContext["Products Price"] = decimal.Parse(Browser.Text(AddProductToCartPage.LABEL_ProductPrice()).Replace("$", ""));
                Console.WriteLine("INFO: Added item to cart with name " + item.name + "and Price is : " + item.unitPrice);
                Assert.AreEqual(Browser.FindElement(AddProductToCartPage.MiniCart_Count()).Text, "1");
            }
        }
    }
}