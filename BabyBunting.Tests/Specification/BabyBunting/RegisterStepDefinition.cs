﻿using BabyBunting.Tests.Specification.BabyBuntingPages;
using BabyBunting.Tests.Specification.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBunting
{
    [Binding]
    public class RegisterStepDefinition : BaseSteps
    {
        [StepDefinition(@"I enter the following in ""Registration"" page")]
        public void WhenIEnterTheFollowingInRegisterPage(Table table)
        {
            var uri = new Uri(Browser.Url);

            if (uri.AbsoluteUri.Contains("#register"))
            {
                var randomString = RandomString.Random(5);
                var currDateTime = DateTime.Now.ToString("MMddyyHmmss");
                foreach (var row in table.Rows)
                {
                    var value = row["Value"].Replace("{RANDOM STRING}", randomString).Replace("{CURR DATE TIME}", currDateTime);
                    switch (row["Field name"].ToLower())
                    {
                        case "first name":
                            Browser.ClearThenSendKeys(RegisterPage.INPUT_FirstName(), value);
                            break;
                        case "last name":
                            Browser.ClearThenSendKeys(RegisterPage.INPUT_LastName(), value);
                            break;
                        case "email address":
                            Browser.ClearThenSendKeys(RegisterPage.INPUT_EmailAddress(), value);
                            break;
                        case "password":
                            Browser.ClearThenSendKeys(RegisterPage.INPUT_Password(), value);
                            break;
                        case "confirm password":
                            Browser.ClearThenSendKeys(RegisterPage.INPUT_ConfirmPassword(), value);
                            break;
                        default:
                            throw new Exception("ERROR: Unrecognized field name of " + row["Field name"]);
                    }
                }
            }
        }
        [StepDefinition(@"I click signup button")]
        public void WhenIClickSignupButton()
        {
            Browser.FindElement(RegisterPage.BTN_SignUP()).Click();
            Browser.WaitElementToDisappear(RegisterPage.BTN_SignUP());
        }
       
    }
}
