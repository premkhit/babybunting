﻿using BabyBunting.Tests.Specification;
using BabyBunting.Tests.Specification.BabyBuntingPages;
using BabyBunting.Tests.Specification.Helper;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace GeneralSpecFlowTemplate
{
    [Binding]
    public class SitecoreStepDefinition : BaseSteps
    {
        [StepDefinition(@"I can see the following login elements")]
        public void ThenICanSeeTheFollowingLoginElements(Table table)
        {
            foreach (var row in table.Rows)
            {
                switch (row["elements"].ToLower())
                {
                    case "sitecore login page":
                        Assert.AreEqual(1, Browser.FindElements(SitecorePage.DIV_SitecoreLogin()).Count);
                        break;

                    case "sitecore cms login page fields":
                        Assert.AreEqual(1, Browser.FindElements(SitecorePage.INPUT_Loginfield()).Count);
                        break;

                    default:
                        throw new Exception("ERROR: Unrecognized element of " + row["elements"]);
                }
            }
        }

        [StepDefinition(@"I login to Sitecore using the following credentials")]
        public void WhenILoginToSitecoreUsingTheFollowingCredentials(Table table)
        {
            // if we haven't navigate to Fulfillment login page, do so
            if (Browser.FindElements(SitecorePage.INPUT_Loginfield()).Count == 0)
                When("I navigate to Fulfillment Login page");

            foreach (var row in table.Rows)
            {
                switch (row["field name"].ToLower())
                {
                    case "username":
                        if (row["value"].ToLower() == "{admin}")
                            Browser.ClearThenSendKeys(SitecorePage.INPUT_Username(), Config.GetRequiredValue("SitecoreLogin.Username"));
                        else
                            Browser.ClearThenSendKeys(SitecorePage.INPUT_Username(), row["value"]);
                        break;

                    case "password":
                        if (row["value"].ToLower() == "{password}")
                            Browser.ClearThenSendKeys(SitecorePage.INPUT_Password(), Config.GetRequiredValue("SitecoreLogin.Password"));
                        else
                            Browser.ClearThenSendKeys(SitecorePage.INPUT_Password(), row["value"]);
                        break;

                    default:
                        throw new Exception("ERROR: Unrecognized field name of " + row["field name"]);
                }
            }
            Browser.Click(SitecorePage.BTN_LogOn());
        }

        [StepDefinition(@"I will be logged in to Sitecore")]
        public void ThenIWillBeLoggedInToSitecore()
        {
            Assert.AreEqual(1, Browser.WaitElements(SitecorePage.ContentEditor.DIV_ApplicationMenu()).Count);
        }

        [StepDefinition(@"I select Content Editor")]
        public void WhenISelectContentEditor()
        {
            Browser.WaitElement(SitecorePage.Login.BTN_ContentEditor()).Click();
        }

        [StepDefinition(@"I can see the following in the content tree")]
        public void ThenICanSeeTheFollowingInTheContentTree(Table table)
        {
            // First we need to ensure that the "Content" node is expand
            //if (Browser.FindElements(SitecorePage.ContentEditor.BTN_ExpandNode("")).Count == 1)
            //{
            //    Browser.Click(SitecorePage.ContentEditor.BTN_ExpandNode("Content"));
            //}

            foreach (var row in table.Rows)
            {
               // Assert.AreEqual(1, Browser.WaitElements(SitecorePage.ContentEditor.LINK_TreeNode(row["node item"])).Count, "ERROR: Cannot find " + row["node item"] + " in Sitecore....");
            }
        }
    }
}
    