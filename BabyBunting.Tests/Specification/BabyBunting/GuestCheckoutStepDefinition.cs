﻿using BabyBunting.Tests.Specification.BabyBuntingPages;
using BabyBunting.Tests.Specification.Helper;
using BunningsPU.Specification;
using BunningsPU.Specification.Helper;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBunting
{
    [Binding]
    public class GuestCheckoutStepDefinition : BaseSteps
    {
        public static Cart shoppingList
        {
            get
            {
                if (!ScenarioContext.Current.ContainsKey("shopping list"))
                {
                    ScenarioContext.Current["shopping list"] = new Cart();
                }
                return (Cart)ScenarioContext.Current["shopping list"];
            }
        }
        [StepDefinition(@"I click on View cart from Mini Cart")]
        public void ThenIClickOnViewCartFromMiniCart()
        {
            Browser.FindElement(CartPage.Hover_MiniCart()).Click();
            Wait.Until(Browser => Browser.FindElement(CartPage.SubtotalPrice()).Displayed);
        }

        [StepDefinition(@"I verify Termando Shipping fee")]
        public void ThenIVerifyTermandoShippingFee()
        {
            var shippingFee = new ShoppingListItem
            {
                termandoFee = decimal.Parse(Browser.Text(CartPage.TermandoShippingFee()).Replace("$", "0")),
                quantity = 1
            };
            ScenarioContext["Termando ShippingFee"] = shippingFee.termandoFee;
            Console.WriteLine("The shipping fee will be " + shippingFee.termandoFee + " as added item is Big And Bulky");
            Assert.AreNotEqual(9, ScenarioContext["Termando ShippingFee"]);
        }


        [StepDefinition(@"I will be in Guest checkout page")]
        public void ThenIWillBeInGuestCheckoutPage()
        {            
            Wait.Until(Browser=>Browser.FindElement(CartPage.BTN_Checkout()));
            Browser.FindElement(CartPage.BTN_Checkout()).Click();
            Wait.Until(ExpectedConditions.ElementIsVisible(CartPage.BTN_GuestCheckout()));
            Browser.FindElement(CartPage.BTN_GuestCheckout()).Click();
            Wait.Until(ExpectedConditions.ElementIsVisible(CartPage.SubtotalPrice()));
            var item1 = new ShoppingListItem
            {
                miniCartSubtotal = decimal.Parse(Browser.Text(CartPage.SubtotalPrice()).Replace("$", "0")),
                quantity = 1
            };
            ScenarioContext["Subtotal price"] = item1.miniCartSubtotal;
            Console.WriteLine("INFO: Checkout subtotal and Total price is : " + item1.miniCartSubtotal + "Info price from PD page is : " + ScenarioContext["Products Price"]);
            Assert.AreEqual(ScenarioContext["Products Price"], ScenarioContext["Subtotal price"]);
            }

        [StepDefinition(@"I will be in Mobile Guest checkout page")]
        public void ThenIWillBeInMobileGuestCheckoutPage()
        {
            Wait.Until(ExpectedConditions.ElementIsVisible(CartPage.BTN_Checkout()));
            Browser.FindElement(CartPage.BTN_Checkout()).Click();
            Wait.Until(Browser=>Browser.WaitElement(CartPage.BTN_GuestCheckout()));
            IWebElement element = Browser.FindElement(CartPage.BTN_GuestCheckout());
            ((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Browser.FindElement(CartPage.BTN_GuestCheckout()).Click();
            Wait.Until(Browser => Browser.WaitElement(CartPage.SubtotalPrice()));
            var item1 = new ShoppingListItem
            {
                miniCartSubtotal = decimal.Parse(Browser.Text(CartPage.SubtotalPrice()).Replace("$", "0")),
                quantity = 1
            };
            ScenarioContext["Subtotal price"] = item1.miniCartSubtotal;
            Console.WriteLine("INFO: Checkout subtotal and Total price is : " + item1.miniCartSubtotal + "Info price from PD page is : " + ScenarioContext["Products Price"]);
// Assert.AreEqual(ScenarioContext["Products Price"], ScenarioContext["Subtotal price"]);
        }
    }
}
