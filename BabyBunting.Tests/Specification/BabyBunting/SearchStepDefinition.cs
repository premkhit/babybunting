﻿using BabyBunting.Tests.Specification.BabyBuntingPages;
using BabyBunting.Tests.Specification.Helper;
using NUnit.Framework;
using System;
using System.Threading;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBunting
{
    [Binding]
    public class SearchStepDefinition : BaseSteps
    {
        //[StepDefinition(@"I click on search icon")]
        //public void GivenIClickOnSearchIcon()
        //{
        //    Browser.FindElement(SearchPage.SearchIcon()).Click();
        //}

        [StepDefinition(@"I click on ""(.*)""")]
        public void WhenINavigate(string button)
        {
            switch (button.ToLower())
            {
                case "searchicon":                    
                    Browser.FindElement(SearchPage.SearchIcon()).Click();
                    break;
                case "popup":
                    Browser.FindElement(SearchPage.PopUp_Close()).Click();
                    break;
                case "sitecore":
                    Browser.Navigate().GoToUrl(BaseUrl + "sitecore");
                    break;
                default:
                    
                    break;
            }
        }


        [StepDefinition(@"I populate search textbox with ""(.*)""")]
        public void GivenIPopulateSearchTextboxWith(string p0)
        {
            Browser.ClearThenSendKeys(SearchPage.INPUT_SearchBox(), p0);
            Thread.Sleep(1000);
        }


        [StepDefinition(@"result page should have search term")] 
        public void ThenResultPageShouldHaveSearchTerm()
        {
            Assert.IsTrue(Browser.WaitElement(SearchPage.SearchHeader()).Displayed);
            //StringAssert.Contains(p0, Browser.Text(SearchPage.SearchHeader()));
           
        }

        [StepDefinition(@"I click mobile search from menu")]
        public void GivenIClickMobileSearchFromMenu()
        {
            Browser.FindElement(SearchPage.MobileSearchIcon()).Click();
        }


    }
}
