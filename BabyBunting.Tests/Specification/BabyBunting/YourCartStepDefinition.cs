﻿using BabyBunting.Tests.Specification.BabyBuntingPages;
using BabyBunting.Tests.Specification.Helper;
using BunningsPU.Specification;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBunting
{
    [Binding]
    public class YourCartStepDefinition : BaseSteps
    {
        [StepDefinition(@"I increase the count of the item")]
        public void GivenIIncreaseTheCountOfTheItem()
        {
            var item1 = new ShoppingListItem
            {
                miniCartSubtotal = decimal.Parse(Browser.Text(CartPage.SubtotalPrice()).Replace("$", "")),
                quantity = 1
            };
            ScenarioContext["Subtotal price1"] = item1.miniCartSubtotal;
            Console.WriteLine("INFO: Checkout subtotal and Total price is : " + item1.miniCartSubtotal + "Info price from PD page is : " + ScenarioContext["Products Price"]);       
            Browser.FindElement(YourCartPage.Increase_ShoppingCart()).Click();
            Thread.Sleep(3000);
        }

        [StepDefinition(@"I see SubTotal Value will change")]
        public void ThenISeeSubTotalValueWillChange()
        {
            var item2 = new ShoppingListItem
            {
                miniCartSubtotal = decimal.Parse(Browser.Text(CartPage.SubtotalPrice()).Replace("$", "")),
                quantity = 1
            };
            ScenarioContext["Subtotal price2"] = item2.miniCartSubtotal;
            Console.WriteLine("INFO: Price of the item After increase =====================" + ScenarioContext["Subtotal price2"]);
            Assert.AreNotSame(ScenarioContext["Subtotal price1"], ScenarioContext["Subtotal price2"]);
            Console.WriteLine("INFO: Price of the item before increase " + ScenarioContext["Subtotal price1"] + " Info price after increase of item " + ScenarioContext["Subtotal price2"]);
        }

        [StepDefinition(@"I decrease the count of the item")]
        public void GivenIDecreaseTheCountOfTheItem()
        {
            Browser.FindElement(YourCartPage.Decrease_ShoppingCart()).Click();
            Thread.Sleep(3000);
        }

        [StepDefinition(@"I remove the item from the cart")]
        public void GivenIRemoveTheItemFromTheCart()
        {
            Browser.FindElement(YourCartPage.RemoveItem_ShoppingCart()).Click();
        }

        [StepDefinition(@"I see Empty Cart page")]
        public void ThenISeeEmptyCartPage()
        {
            Wait.Until(Browser=> Browser.FindElement(YourCartPage.Empty_ShoppingCart()).Displayed);
            Assert.IsTrue(Browser.FindElement(YourCartPage.Empty_ShoppingCart()).Displayed);
        }

        [StepDefinition(@"I see SubTotal Value gets updated")]
        public void ThenISeeSubTotalValueGetsUpdated()
        {
            Thread.Sleep(3000);
            var item3 = new ShoppingListItem
            {
                miniCartSubtotal = decimal.Parse(Browser.Text(CartPage.SubtotalPrice()).Replace("$", "")),
                quantity = 1
            };
            ScenarioContext["Subtotal price3"] = item3.miniCartSubtotal;
            Console.WriteLine("INFO: Price of the item After decrease =====================" + ScenarioContext["Subtotal price3"]);
            Console.WriteLine("INFO: Price of the item " + ScenarioContext["Subtotal price1"] + " Info price after decrease of item " + ScenarioContext["Subtotal price3"]);
            Assert.AreEqual(ScenarioContext["Subtotal price1"], ScenarioContext["Subtotal price3"]);
            
        }
        [StepDefinition(@"I apply valid promocode (.*)")]
        public void GivenIApplyValidPromocode(String Code)
        {
            Browser.ClearThenSendKeys(YourCartPage.INPUT_PromoCode(), Code);
            // Browser.WaitElement(YourCartPage.BTN_APPLY()).Enabled);
            Browser.Sleep(2000);
            
        }
        [StepDefinition(@"I see the discount added to cart")]
        public void ThenISeeTheDiscountAddedToCart()
        {
            Browser.FindElement(YourCartPage.BTN_APPLY()).Click();
            Wait.Until(Browser => Browser.ElementExists(YourCartPage.TXT_PromotionDiscount()));
        }

        [StepDefinition(@"i see the (.*)")]
        public void ThenISeeTheStoreName(String value)
        {
            switch (value.ToLower())
            {
                case "store name":
                    Assert.IsTrue(Browser.FindElement(YourCartPage.Badge_Store()).Displayed);
                    break;
                case "color variant":
                    Assert.IsTrue(Browser.FindElement(YourCartPage.Variant_Color()).Displayed);
                    Assert.IsTrue(Browser.FindElement(YourCartPage.Variant_Size()).Displayed);
                    break;
                default:
                    break;
            }
        }

    }
}
