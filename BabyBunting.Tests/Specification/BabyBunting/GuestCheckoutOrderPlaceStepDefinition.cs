﻿using BabyBunting.Tests.Specification.BabyBuntingPages;
using BabyBunting.Tests.Specification.Helper;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBunting
{
    [Binding]
    public class GuestCheckoutOrderPlaceStepDefinition : BaseSteps
    {
        [Then(@"I populate address value with ""(.*)""")]
        public void ThenIPopulateAddressValueWith(string Address)
        {
            // Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.Address(), Address);
           // new WebDriverWait(Browser, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementExists(GuestCheckoutOrderPlace.AutoSuggestion(Address)));

        }

        [StepDefinition(@"I click on link enter address manual")]
        public void ThenIClickOnEnterAddressManualLink()
        {
            Wait.Until(ExpectedConditions.ElementIsVisible(GuestCheckoutOrderPlace.LINK_ManualAdress()));
            Browser.FindElement(GuestCheckoutOrderPlace.LINK_ManualAdress()).SendKeys(Keys.Enter);
        }
        [StepDefinition(@"I enter the select After Pay in Payment section")]
        [StepDefinition(@"I enter the select Zip Pay in Payment section")]
        [StepDefinition(@"I enter the following in Click and Collect order Billing section")]
        [StepDefinition(@"I enter the following in Billing section")]
        [StepDefinition(@"I enter the following in Send Your goodies section")]
        public void WhenIEnterTheFollowingInSendYourGoodiesSection(Table table)
        {
            foreach (var row in table.Rows)
            {
                var value = row["Value"];
                switch (row["Field name"].ToLower())
                {
                    case "first name":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_FirstName(), value);
                        break;
                    case "last name":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_LastName(), value);
                        break;
                    case "address":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_Address(), value);
                        break;
                    case "city":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_City(), value);
                        break;
                    case "state":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_State(), value);
                        break;
                    case "postcode":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_Postcode(), value);
                        break;
                    case "phone number":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_PhoneNumber(), value);
                        Browser.FindElement(GuestCheckoutOrderPlace.INPUT_PhoneNumber()).SendKeys(Keys.Tab);
                        Wait.Until(ExpectedConditions.ElementToBeClickable(GuestCheckoutOrderPlace.BTN_NextGetShipping())).SendKeys(Keys.Enter);
                        Browser.FindElement(GuestCheckoutOrderPlace.BTN_NextGetShipping()).SendKeys(Keys.Enter);
                        break;
                    case "email1":
                        Wait.Until(Browser => Browser.FindElement(GuestCheckoutOrderPlace.INPUT_Email()));
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_Email(), value);
                        break;
                    case "email":
                        Wait.Until(Browser => Browser.FindElement(GuestCheckoutOrderPlace.INPUT_Email()));
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_Email(), value);
                        Thread.Sleep(5000);
                        IWebElement element1 = Browser.FindElement(By.CssSelector("label[for='paymentOption0']"));
                        ((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].scrollIntoView(true);", element1);
                        //Wait.Until(Browser => Browser.FindElement(By.CssSelector("label[for='paymentOption0']")).Displayed);
                        Browser.FindElement(By.CssSelector("label[for='paymentOption0']")).Click();
                        break;
                    case "credit card":
                         Browser.SwitchTo().DefaultContent();
                        // WebDriverWait wait = new WebDriverWait(Browser, new TimeSpan(0,0,10));
                        Thread.Sleep(2000);
                        // wait.Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(By.Id("braintree-dropin-frame")));                        
                        Browser.SwitchTo().Frame("braintree-dropin-frame");
                        var element = Browser.FindElement(By.Id("credit-card-number"));
                        element.SendKeys(value);
                        break;
                    case "expiration":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_ExpiryDate(), value);
                        break;
                    case "cvv":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_CVV(), value);
                        Browser.SwitchTo().DefaultContent();
                        Browser.FindElement(GuestCheckoutOrderPlace.BTN_ConfirmPayment()).Click();
                        Wait.Until(Browser =>Browser.FindElement(GuestCheckoutOrderPlace.BTN_PlaceOrder()).Displayed);
                        break;
                    case "cc first name":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_CCFirstName(), value);
                        break;
                    case "cc last name":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_CCLastName(), value);
                        break;
                    case "cc address":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_CCAddress(), value);
                        break;
                    case "cc city":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_CCCity(), value);
                        break;
                    case "cc state":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_CCState(), value);
                        break;
                    case "cc postcode":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_CCPostcode(), value);
                        break;
                    case "cc phone number":
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_CCPhoneNumber(), value);
                        //Browser.FindElement(GuestCheckoutOrderPlace.BTN_NextGetShipping()).SendKeys(Keys.Enter);
                        break;
                    case "zip pay email":
                        Wait.Until(Browser => Browser.FindElement(GuestCheckoutOrderPlace.INPUT_Email()).Displayed);
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_Email(), value);
                        Browser.Sleep(3000);
                        IWebElement element2 = Browser.FindElement(By.CssSelector("label[for='paymentOption1']"));
                        ((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].scrollIntoView(true);", element2);
                        Browser.FindElement(By.CssSelector("label[for='paymentOption1']")).Click();
                        break;
                    case "after pay email":
                        Wait.Until(Browser => Browser.FindElement(GuestCheckoutOrderPlace.INPUT_Email()).Displayed);
                        Browser.ClearThenSendKeys(GuestCheckoutOrderPlace.INPUT_Email(), value);
                        Browser.Sleep(3000);
                        IWebElement element3 = Browser.FindElement(By.CssSelector("label[for='paymentOption2']"));
                        ((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].scrollIntoView(true);", element3);
                        Browser.FindElement(By.CssSelector("label[for='paymentOption2']")).Click();
                        break;
                    default:
                        throw new Exception("ERROR: Unrecognized field name of " + row["Field name"]);
                }
                  
            }
        }
        [StepDefinition(@"I see place order button")]
        public void ThenISeePlaceOrderButton()
        {
            Wait.Until(Browser => Browser.FindElement(GuestCheckoutOrderPlace.BTN_PlaceOrder()).Displayed);
            Assert.IsTrue(Browser.FindElement(GuestCheckoutOrderPlace.BTN_PlaceOrder()).Displayed);
        }

        [StepDefinition(@"I select shipping option")]
        public void WhenISelectShippingOption()
        {
            Browser.Sleep(5000);
            Wait.Until(Browser=>Browser.FindElement(GuestCheckoutOrderPlace.Radio_Shipping()).Displayed);
           // Browser.FindElement(GuestCheckoutOrderPlace.Radio_Shipping()).Click();
            Browser.FindElement(GuestCheckoutOrderPlace.BTN_NextGetBilling()).SendKeys(Keys.Enter);
            Wait.Until(Browser => Browser.FindElement(By.CssSelector("label[for='e016d519-8308-409c-a05c-745ca3bb12d3']")).Displayed);
        }
        [StepDefinition(@"I select Mobile shipping option")]
        public void WhenISelectMobileShippingOption()
        {
            Browser.Sleep(5000);
            Wait.Until(Browser => Browser.FindElement(GuestCheckoutOrderPlace.Radio_Shipping()).Displayed);
            // IWebElement element = Browser.FindElement(GuestCheckoutOrderPlace.BTN_NextGetBilling());
            // ((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            // Browser.FindElement(GuestCheckoutOrderPlace.Radio_Shipping()).Click();
            Browser.FindElement(GuestCheckoutOrderPlace.BTN_NextGetBilling()).SendKeys(Keys.Enter);
        }

        [StepDefinition(@"I Click Next button for ClickAndCollect shipping option")]
        public void WhenIClickNextButtonForClickAndCollectShippingOption()
        {
            //Wait.Until(Browser => Browser.FindElement(GuestCheckoutOrderPlace.BTN_NextGetShipping()));
            //Browser.FindElement(GuestCheckoutOrderPlace.BTN_NextGetShipping()).SendKeys(Keys.Enter);
            Wait.Until(Browser=>Browser.FindElement(GuestCheckoutOrderPlace.BTN_NextGetBilling()));
            Browser.FindElement(GuestCheckoutOrderPlace.BTN_NextGetBilling()).SendKeys(Keys.Enter);
        }

        [StepDefinition(@"I will be in Order Confirmation page")]
        public void ThenIWillBeInOrderConfirmationPage()
        {
            if (Browser.Url.Contains("/checkout/orderconfirmation?"))
                Assert.IsTrue(Browser.FindElement(GuestCheckoutOrderPlace.Txt_OrderConfirmation()).Displayed);
            else
            {
                Assert.IsTrue(Browser.FindElement(GuestCheckoutOrderPlace.Txt_CheckoutPage()).Displayed);
            }
        }
        [When(@"I click Place Order button")]
        public void WhenIClickPlaceOrderButton()
        {
            if (Browser.Url.Contains("https://cd-dev.babybunting.com.au"))
            {
                Browser.FindElement(GuestCheckoutOrderPlace.BTN_PlaceOrder()).Click();
                Browser.WaitForUrlToLoad("/checkout/orderconfirmation?");
            }

            else
            {
                Assert.IsTrue(Browser.FindElement(GuestCheckoutOrderPlace.BTN_PlaceOrder()).Displayed);
            }
                
        }

        [StepDefinition(@"I see (.*) payment section")]
        public void WhenISeePaymentSection(String payment)
        {
            switch (payment.ToLower())
            {
                case "zip pay":
                    Wait.Until(Browser => Browser.FindElement(By.CssSelector("label[for='e016d519-8308-409c-a05c-745ca3bb12d3']")));
                    IWebElement element2 = Browser.FindElement(By.CssSelector("label[for='e016d519-8308-409c-a05c-745ca3bb12d3']"));
                    ((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].scrollIntoView(true);", element2);
                    Browser.FindElement(By.CssSelector("label[for='e016d519-8308-409c-a05c-745ca3bb12d3']")).Click();
                    break;
                case "after pay":
                    Wait.Until(Browser => Browser.FindElement(By.CssSelector("label[for='0cffab11-2674-4a18-ab04-228b1f8a1dec']")).Displayed) ;
                    IWebElement element3 = Browser.FindElement(By.CssSelector("label[for='9c77f86c-184f-4b9a-9459-4373a0f39ac8']"));
                    ((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].scrollIntoView(true);", element3);
                    Browser.FindElement(By.CssSelector("label[for='9c77f86c-184f-4b9a-9459-4373a0f39ac8']")).Click();
                    break;
                case "credit card":
                    Thread.Sleep(3000);
                    Wait.Until(Browser => Browser.FindElement(By.CssSelector("label[for='0cffab11-2674-4a18-ab04-228b1f8a1dec']")));
                    IWebElement element1 = Browser.FindElement(By.CssSelector("label[for='0cffab11-2674-4a18-ab04-228b1f8a1dec']"));
                    ((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].scrollIntoView(true);", element1);
                    //Wait.Until(Browser => Browser.FindElement(By.CssSelector("label[for='paymentOption0']")).Displayed);
                    Browser.FindElement(By.CssSelector("label[for='0cffab11-2674-4a18-ab04-228b1f8a1dec']")).Click();
                    break;
                default:
                    Browser.Navigate().GoToUrl(BaseUrl + payment);
                    break;

            }
            
        }
    }
}
