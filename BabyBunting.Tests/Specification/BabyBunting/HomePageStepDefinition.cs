﻿using System;
using System.Linq;
using System.Net;
using TechTalk.SpecFlow;
using BabyBunting.Tests.Specification.BabyBuntingPages;
using NUnit.Framework;
using OpenQA.Selenium;

namespace BabyBunting.Tests.Specification.BabyBunting
{

    [Binding]
    public class HomePageStepDefinition : BaseSteps
    {
        private HttpWebRequest task;
        [StepDefinition(@"I will see (.*) page")]
        public void ThenIWillSeePage(string page)
        {
            switch (page.ToLower())
            {
                case "category":
                    Assert.IsTrue(Browser.FindElement(ProductListingPage.CategoriesSubNav()).Displayed);
                    break;
                case "product":
                    Assert.IsTrue(Browser.FindElement(AddProductToCartPage.ProductName()).Displayed);
                    break;
                case "call out default":
                    Assert.IsTrue(Browser.FindElement(HomePage.COMPONENT_CallOutDefault()).Displayed);
                    break;
                default:
                    Browser.Navigate().GoToUrl(BaseUrl + page);
                    task = (HttpWebRequest)WebRequest.Create(BaseUrl + page);
                    break;

            }
        }

        [StepDefinition(@"i see home page ""(.*)"" component")]
        public void ThenISeeHomePageComponent(string component)
        {
            switch (component.ToLower())
            {
                case "mini tile":
                    Assert.IsTrue(Browser.FindElement(HomePage.COMPONENT_MiniTile()).Displayed);
                    break;
                case "slider":
                    Assert.IsTrue(Browser.FindElement(HomePage.COMPONENT_Slider()).Displayed);
                    break;
                case "call out default":
                    //IWebElement element = Browser.FindElement(HomePage.COMPONENT_CallOutDefault());
                    //((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].scrollIntoView(true);", element);
                    Assert.IsTrue(Browser.FindElement(HomePage.COMPONENT_CallOutDefault()).Displayed);
                    break;
                case "call out tiles":
                    Assert.IsTrue(Browser.FindElement(HomePage.COMPONENT_CallOutTiles()).Displayed);
                    break;
                case "category tab":
                    Assert.IsTrue(Browser.FindElement(HomePage.COMPONENT_CategoryTab()).Displayed);
                        break;
                case "brand carousel":
                    Assert.IsTrue(Browser.FindElement(HomePage.COMPONENT_BrandCarousel()).Displayed);
                    break;
                case "cta carousel":
                    Assert.IsTrue(Browser.FindElement(HomePage.COMPONENT_CTACarousel()).Displayed);
                    break;
                default:
                    Browser.Navigate().GoToUrl(BaseUrl + component);
                    task = (HttpWebRequest)WebRequest.Create(BaseUrl + component);
                    break;
            }
        }
          [StepDefinition(@"I select category (.*)")]
          [StepDefinition(@"I click on following (.*) navi")]
        public void WhenIClickOnFollowingNavi(string Category)
        {
            Browser.FindElement(HomePage.HEADER_CAT(Category)).Click();
        }
        [StepDefinition(@"I click on following for (.*) mobile")]
        public void WhenIClickOnFollowingForMobile(String Category)
        {
            Wait.Until(Browser => Browser.FindElement(HomePage.Mobile_CATNav(Category)).Displayed);
            Browser.FindElement(HomePage.Mobile_CATNav(Category)).Click();
           // Wait.Until(Browser=>Browser.FindElement(By.XPath("//div[@class='mobile-nav__sub-nav-wrapper']//div[@class='menu-link field-navigationtitle']/a[@href='/shop/prams-strollers=babybunting-413']")).Displayed);
           // Browser.FindElement(By.XPath("//div[@class='mobile-nav__sub-nav-wrapper']//div[@class='menu-link field-navigationtitle']/a[@href='/shop/prams-strollers=babybunting-413']")).Click();
        }
        [StepDefinition(@"the correct (.*) will show up in header section")]
        public void ThenTheCorrectWillShowUpInHeaderSection(string p0)
        {
            Assert.AreEqual(Browser.FindElement(HomePage.LABEL_CATEGORY_NAME()).Text, p0);
            
        }
        [StepDefinition(@"I click on Menu nav")]
        public void GivenIClickOnMenuNav()
        {
            Browser.FindElement(HomePage.Menu_Mobile()).Click();
            Wait.Until(Browser => Browser.FindElement(By.XPath("//div[@class='mobile-nav__sub-nav-wrapper']//div[@class='menu-link field-navigationtitle']/a[@title='Home']")).Displayed);
            Browser.FindElement(By.XPath("//div[@class='mobile-nav__sub-nav-wrapper']//a[@href='/']")).Click();
        }

        [StepDefinition(@"I can see Show more button")]
        public void ThenICanSeeShowMoreButton()
        {
            Assert.IsTrue(Browser.FindElement(ProductListingPage.BTN_LoadMore()).Displayed);
            Browser.FindElement(ProductListingPage.BTN_LoadMore()).Click();
        }
// [StepDefinition(@"I randomly select one of the (.*)")]
        [StepDefinition(@"I randomly select one of the (.*) tile")]
        public void WhenIRandomlySelectOneOfTheTile(string component)
        {
            {
                switch (component.ToLower())
                {
                    case "category mini":
                        Wait.Until(Browser =>Browser.FindElement(HomePage.HomePage_MiniCategoryTileList()));
                        var CategoryMiniTile = Browser.FindElements(HomePage.HomePage_MiniCategoryTileList()).ToList();
                        if (CategoryMiniTile.Count == 0)
                            throw new Exception("ERROR: Cannot select any category mini tile ,,,,");
                        var categoryMiniTile = CategoryMiniTile[(new Random()).Next(0, CategoryMiniTile.Count())];
                        categoryMiniTile.Click();
                        break;
                    case "product slider":
                        Wait.Until(Browser => Browser.FindElement(HomePage.HomePage_ProductSliderList()));
                        var SliderProduct = Browser.FindElements(HomePage.HomePage_ProductSliderList()).ToList();
                        if (SliderProduct.Count == 0)
                            throw new Exception("ERROR: Cannot select any productslider tile ,,,,");
                        var sliderProduct = SliderProduct[(new Random()).Next(0, SliderProduct.Count())];
                        sliderProduct.Click();
                        break;
                    case "call out default":
                        var DefaultCallOutTile = Browser.FindElements(HomePage.HomePage_DefaultCallOutTile()).ToList();
                        if (DefaultCallOutTile.Count == 0)
                            throw new Exception("ERROR: Cannot select any productslider tile ,,,,");
                        var defaultCallOutTile = DefaultCallOutTile[(new Random()).Next(0, DefaultCallOutTile.Count())];
                        defaultCallOutTile.Click();
                        break;
                    case "call out":
                        IWebElement element1 = Browser.FindElement(HomePage.HomePage_DefaultCallOutTile());
                        ((IJavaScriptExecutor)Browser).ExecuteScript("arguments[0].scrollIntoView(true);", element1);
                        var CallOutTile = Browser.FindElements(HomePage.HomePage_DefaultCallOutTile()).ToList();
                        if (CallOutTile.Count == 0)
                            throw new Exception("ERROR: Cannot select any callout tile ,,,,");
                        var callOutTile = CallOutTile[(new Random()).Next(0, CallOutTile.Count())];
                        callOutTile.Click();
                        break;
                    case "tab":
                        var Tab = Browser.FindElements(HomePage.HomePage_Tab()).ToList();
                        if (Tab.Count == 0)
                            throw new Exception("ERROR: Cannot select any productslider tile ,,,,");
                        var tab = Tab[(new Random()).Next(0, Tab.Count())];
                        tab.Click();
                        break;
                    case "product category tab":
                        var ProductTab = Browser.FindElements(HomePage.HomePage_TabProductTile()).ToList();
                        if (ProductTab.Count == 0)
                            throw new Exception("ERROR: Cannot select any productslider tile ,,,,");
                        var productTab = ProductTab[(new Random()).Next(0, ProductTab.Count())];
                        productTab.Click();
                        break;
                    case "brand carousel":
                        Assert.IsTrue(Browser.FindElement(ArticlePage.ALCOMP_ListedArticle()).Displayed);
                        break;
                     default:
                        throw new Exception("ERROR: Unrecognized field name of " + component);
                }
            }
           
        }

    }
}