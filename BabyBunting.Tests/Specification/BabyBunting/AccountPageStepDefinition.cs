﻿using BabyBunting.Tests.Specification.BabyBuntingPages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace BabyBunting.Tests.Specification.BabyBunting
{
    [Binding]
    class AccountPageStepDefinition : BaseSteps
    {
        private HttpWebRequest task; //For Calling the page
        private readonly HttpWebResponse taskresponse = null; //Response returned
        [StepDefinition(@"I can see section to edit (.*)")]
        public void ThenICanSeeSectionToEditProfilePage(String account)
        {
            switch (account.ToLower())
            {
                case "profile":
                    Assert.IsTrue(Browser.FindElement(AccountPage.DIV_EditProfile()).Displayed);
                    Assert.IsTrue(Browser.FindElement(AccountPage.DIV_EditChangePassword()).Displayed);
                    break;
                case "address & delivery":
                    Assert.IsTrue(Browser.FindElement(AccountPage.DIV_AddressesAndDelivery()).Displayed);
                    break;
                case "order history":
                    Assert.IsTrue(Browser.FindElement(AccountPage.DIV_OrderHistory()).Displayed);
                    break;
                default:
                    Browser.Navigate().GoToUrl(BaseUrl + account);
                    task = (HttpWebRequest)WebRequest.Create(BaseUrl + account);
                    break;
            }
        }

        [StepDefinition(@"I click on (.*) link")]
        public void WhenIClickOnLink(String AccountsLink)
        {
            Wait.Until(Browser => Browser.FindElement(AccountPage.Link_AccountMenu(AccountsLink)));
            Browser.FindElement(AccountPage.Link_AccountMenu(AccountsLink)).Click();

        }

    }
}
