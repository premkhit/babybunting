﻿@BBCD @Desktop 
Feature: Product detail page
	These tests are to test the website ability to perform actions on Product details page

Scenario: Can visit product details page directly going to Product detail page
	Given I am at ProductDetailPage
		Then I will be in Product detail page 

Scenario: Can visit the product detail page from category listing page from category listing page
	Given I am at bbhomepage
		When I select category {4B5C4960-DD73-2BA4-D883-41C25F3D49A9} 
		Then I will be in product listing page
	When I select randomly one of the product in the product listing
		Then I will be in Product detail page
		# And I add product to cart

Scenario: Can add Big And Bulky item to cart 
	Given I am at Big and Bulky PD page
		Then I will be in Product detail page
		And I add product to cart
		When I click on View cart from Mini Cart
		Then I verify Termando Shipping fee