﻿@BBCM @Desktop
Feature: Content Management
	This feature is to test the BabyBunting CM (Content Management) side such as login to Sitecore
@mytag
Scenario: Test that we can successfully login to Sitecore as Editor users
	When I navigate to sitecore
	Then I can see the following login elements
	| elements                       |
	| sitecore login page            |
	| sitecore cms login page fields |
	When I login to Sitecore using the following credentials
	| field name | value      |
	| username   | {admin}    |
	| password   | {password} |
	Then I will be logged in to Sitecore 