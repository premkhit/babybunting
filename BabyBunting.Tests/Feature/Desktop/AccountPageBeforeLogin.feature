﻿@BBCD @Desktop
Feature: AccountPageBeforeLogin
	This feature is to verify SignIn/SignUp Functionality

@ignore
Scenario: Check that the page displays both Sign In and Create Account user tab component
	Given I navigate to before login Account page
	Then I can see the following options
	| options |
	|  Create an account        |
	|   Sign In       |
	When I click on create an account button
	Then I will see form Create Account form
	When I click on Sign In button
	Then I will see form Sign In form

@ignore 
Scenario: Check Create new BB account functionality
Given I navigate to Create an account page
Then I enter the following in "create an account" page
| Field name        | Value                            |
| first name        | first{RANDOM STRING}             |
| last name         | last{RANDOM STRING}              |
| email address     | premkhit{RANDOM STRING}@test.com |
| set your password | Newuser@123                      |
	And I click on Receive email checkbox
	When I click on Create account button
	Then I will be redirected to Profile page

Scenario: Check BB Sign In account functionality
        Given I navigate to login page
          Then I enter the following in sign in page
        | Field name        | Value                 |
        | registered email    | premkhit@test6.com |
        | registered password | 12345678            |
         When I click on Sign in button
	     Then I can see user is logged in

@ignore
 Scenario: Check BB Sign In account functionality using Google account
          Given I navigate to login page
             When I click on google account button
             Then I enter the following in google email address page
             | Field name        | Value                            |
             | google email address     | loudclearbaby@gmail.com |
                And I click on Google Next button
              Then I enter the following in google password page
              | Field name        | Value                            |
              | google password     | Qaz1Qaz2 |
			  And I click on Google Next button
	         # When I click on Google Sign in button
	         Then I can see user is logged in