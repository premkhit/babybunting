﻿@BBCD @Desktop
Feature: GuestCheckout ZipPay/AfterPay


Background: 
Given I am at ProductDetailPage
		Then I will be in Product detail page 
		And the product is added to cart
		When I click on View cart from Mini Cart
       Then I will be in Guest checkout page
	   #And I populate address value with "83 William Street, Melbourne VIC, Australia"
	   And I click on link enter address manual
	   When I enter the following in Send Your goodies section
	    | Field name | Value             |
	    | First Name | Premkhit          |
	    | Last Name  | Lepcha            |
	    | Address    | 83 William Street |
	    | City       | Melbourne         |
	    | Postcode   | 3000              |
	    | State      | Vic               |
		| Email1       | premkhit@gmail.com |
	    | Phone Number|  0409090909     |
		When I select shipping option

Scenario: Can select ZipPay as payment option
   When I see Zip Pay payment section
   Then I see place order button

Scenario: Can select AfterPay as payment option
   When I see After Pay payment section
   Then I see place order button