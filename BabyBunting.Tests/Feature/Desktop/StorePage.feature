﻿@BBCD @Desktop 
Feature: Store Page
	
Scenario: Test user able to navigate to Store page
Given I navigate to bbhomepage
	Given I navigate to Store page
	Then I can see the following options in Store page
	| options              |
	| Find Component Store |
	| Stores Accordion     |
	| Nearest Stores       |      
	
Scenario: Test user able to navigate to store details page
Given I navigate to bbhomepage
Given I navigate to Store page
When I Randomly Select One Of Store From Nearest Stores Tiles
Then I will be in Store detail page

