﻿@BBCD @Desktop
Feature: AccountPage
	
	Background: 
	 Given I navigate to login page
          Then I enter the following in sign in page
        | Field name        | Value                 |
        | registered email    | premkhit@test6.com |
        | registered password | 12345678            |
         When I click on Sign in button
	     Then I can see user is logged in

Scenario: Test user can visit Edit profile page 
    When I navigate to profile page
	Then I can see section to edit Profile 

Scenario: Test user can visit Address & Delivery page 
    When I navigate to profile page
	And I click on Addresses & Delivery link
	Then I can see section to edit Address & Delivery 

Scenario: Test user can visit Order History page 
    When I navigate to profile page
	And I click on Orders & Tracking link
	Then I can see section to edit Order History