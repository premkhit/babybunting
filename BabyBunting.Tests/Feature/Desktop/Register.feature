﻿@BBCD @Desktop 
Feature: Register 
This is to check registration process

@ignore
Scenario: This is to check that user can register successfully with valid data
	Given I navigate to Registration page
	When I enter the following in "Registration" page
	    | Field name       | Value                        |
		| first name       | first{RANDOM STRING}         |
	    | last name        | last{RANDOM STRING}          |
	    | email address    | prem{RANDOM STRING}@test.com |
	    | password         | 12345678                     |
	    | confirm password | 12345678                     |
	And I click signup button
	Then I can see user is logged in

