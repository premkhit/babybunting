﻿@BBCD @Desktop
Feature: HomePage
	This feature is to verify the components available in homepage


Scenario: Verify Home page Category MiniTile component
	Given I navigate to bbhomepage
	Then i see home page "mini tile" component
	When I randomly select one of the category mini tile
	Then I will see category page

@ignore
Scenario: Verify Home page Category Slider component
	Given I navigate to bbhomepage
	Then i see home page "slider" component
	When I randomly select one of the product slider tile
	Then I will see product page
@ignore
Scenario: Verify Home page Call Out default component
	Given I navigate to bbhomepage
	Then i see home page "Call Out Default" component
		And I randomly select one of the call out default tile

Scenario: Verify Home page Call Out Tiles component
	Given I navigate to bbhomepage
	Then i see home page "Call Out Tiles" component
	 And I randomly select one of the call out tile

Scenario: Verify Home page Category tabs component
	Given I navigate to bbhomepage
	Then i see home page "category tab" component
	#And I randomly select one of the tab 
	When I randomly select one of the product category tab tile
	Then I will see product page
	
Scenario: Verify Home page Brand Carousel component
	Given I navigate to bbhomepage
	Then i see home page "brand carousel" component

Scenario: Verify Home page CTA Carousel component
	Given I navigate to bbhomepage
	Then i see home page "cta carousel" component