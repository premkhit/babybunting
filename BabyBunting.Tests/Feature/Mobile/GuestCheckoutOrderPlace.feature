﻿@BBCD @Mobile
Feature: Mobile GuestUser placing order with Credit Card

Scenario: Check Mobile user able to go through checkout process for deliver Order
 Given I am at ProductDetailPage
		Then I will be in Product detail page 
		And the product is added to cart
		When I click on View cart from Mini Cart		
       Then I will be in Mobile Guest checkout page
	   #And I populate address value with "83 William Street, Melbourne VIC, Australia"
	   And I click on link enter address manual
	   When I enter the following in Send Your goodies section
	    | Field name | Value             |
	    | First Name | Premkhit          |
	    | Last Name  | Lepcha            |
	    | Address    | 83 William Street |
	    | City       | Melbourne         |
	    | Postcode   | 3000              |
	    | State      | Vic               |
		| Email1       | premkhit@gmail.com |
	    | Phone Number|  0409090909     |
		When I select shipping option
		When I see Credit Card payment section
		When I enter the following in Billing section
		| Field name  | Value              |
		| Credit Card | 5555555555554444   |
		| Expiration  | 02/22              |
		| CVV         | 123                |
		And I click on Place Order button
		Then I will be in Order Confirmation page

Scenario: Check Mobile user able to go through checkout process for Click and Collect Order
 Given I am at ProductDetailPage
		Then I will be in Product detail page 
		And I can add C&C to cart page
		When I click on View cart from Mini Cart
		Then I will be in Mobile Guest checkout page
		When I Click Next button for ClickAndCollect shipping option
		And I click on link enter address manual 
		When I enter the following in Click and Collect order Billing section
	    | Field name | Value             |
	    | CC First Name | Premkhit          |
	    | CC Last Name  | Lepcha            |
	    | CC Address    | 83 William Street |
	    | CC City       | Melbourne         |
	    | CC Postcode   | 3000              |
	    | CC State      | Vic               |
	    | CC Phone Number|  0409090909     |
		| Email       | premkhit@gmail.com |
		| Credit Card | 4111 1111 1111 1111  |
		| Expiration  | 02/22              |
		| CVV         | 123                |
		And I click on Place Order button
		Then I will be in Order Confirmation page
	   


