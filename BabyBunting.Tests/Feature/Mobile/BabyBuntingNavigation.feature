﻿@BBCD @Mobile
Feature: BabyBuntingNavigation
	 This is to check Navigation feature

Scenario: Test mobile user can navigate to Home Page
	Given I navigate to bbhomepage

Scenario Outline: Test mobile user can navigate to Category pages
	Given I navigate to mobilehomepage
	And I click on Menu nav
	When I click on following for <CateNav> mobile
	Then the correct <Category Name> will show up in header section 
	#And I can see Show more button

Examples: 
| CateNav    | Category Name   |
| /shop/prams-strollers=babybunting-413      | Prams Strollers |
| /shop/car-seats=babybunting-438   | Car Seats       |
| /shop/carriers=babybunting-447   | Carriers        |
| /shop/safety=babybunting-457    | Safety          |
| /shop/manchester=babybunting-497 | Manchester      |
| /shop/feeding=babybunting-543   | Feeding         |
| /shop/babywear=babybunting-578   | Babywear        |
| /shop/toys=babybunting-603      | Toys            |
| /shop/changing=babybunting-640  | Changing        |
| /shop/nursery=babybunting-663   | Nursery         |

