﻿@BBCD @Mobile
Feature: Mobile Search
	This feature is to perform Search function

Scenario: Test search results listing for mobile
	Given I navigate to bbhomepage
	And I click mobile search from menu
	And I populate search textbox with "Pram"
    And I click on "searchicon"
	Then result page should have search term
