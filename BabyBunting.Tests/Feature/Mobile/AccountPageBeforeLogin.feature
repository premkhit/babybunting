﻿@BBCD @Mobile
Feature: AccountPageBeforeLogin
	This feature is to verify SignIn/SignUp Functionality

Scenario: Check Mobile BB Sign In account functionality
        Given I navigate to login page
          Then I enter the following in sign in page
        | Field name        | Value                 |
        | registered email    | premkhit@test6.com |
        | registered password | 12345678            |
         When I click on Sign in button
	    # Then I can see user is logged in