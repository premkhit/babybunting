﻿@BBCD @Mobile
Feature: Product detail page for mobile
	These tests are to test the website ability to perform actions on Product details page

Scenario: Can visit mobile product details page directly going to Product detail page
	Given I am at ProductDetailPage
		Then I will be in Product detail page 

Scenario: Can visit mobile product detail page from category listing page from category listing page
	Given I navigate to bbhomepage
	Then i see home page "mini tile" component
	When I randomly select one of the category mini tile
		Then I will be in product listing page
	When I select randomly one of the product in the product listing
		Then I will be in Product detail page
		# And I add product to cart

Scenario: Can add Big And Bulky item to cart in mobile
	Given I am at Big and Bulky PD page
		Then I will be in Product detail page
		And I add product to cart
		When I click on View cart from Mini Cart
		Then I verify Termando Shipping fee