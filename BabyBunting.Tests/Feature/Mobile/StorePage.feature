﻿@BBCD @Mobile
Feature: Store Page
	
Scenario: Test mobile user able to navigate to Store page
	Given I navigate to Store page
	Then I can see the following options in Store page
	| options              |
	| Find Component Store |
	| Nearest Stores       |      
	
Scenario: Test mobile user able to navigate to store details page
Given I navigate to Store page
When I Randomly Select One Of Store From Nearest Stores Tiles
Then I will be in Store detail page

