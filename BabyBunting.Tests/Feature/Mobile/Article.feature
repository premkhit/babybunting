﻿@BBCD @Mobile
Feature: Article Page 

Scenario: Test Mobile user can visit Article baby talk page
Given I navigate to babytalk page
Then i see following component
| section          |
| header banner    |
| selected article |
| listed article   |


Scenario: Test Mobile user can visit Article landing page
Given I navigate to babytalk article 
Then i see following component
| section            |
| article hero       |
| article breadcrumb |
| article summary    |
| article rich text  |
| share this         |

