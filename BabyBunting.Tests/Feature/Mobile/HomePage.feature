﻿@BBCD @Mobile
Feature: HomePage Mobile
	This feature is to verify the components available in mobile homepage


Scenario: Verify Home page Mobile Category MiniTile component
	Given I navigate to bbhomepage
	Then i see home page "mini tile" component
	When I randomly select one of the category mini tile
	Then I will see category page
	
Scenario: Verify Home page Mobile Brand Carousel component
	Given I navigate to bbhomepage
	Then i see home page "brand carousel" component
