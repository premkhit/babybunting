﻿@BBCD @Mobile
Feature: Mobile YourCart

Background: 
Given I am at ProductDetailPage
		Then I will be in Product detail page 
		And the product is added to cart
		When I click on View cart from Mini Cart

Scenario: Test Mobile user able to increase the item in the cart
	And I increase the count of the item
	Then I see SubTotal Value will change

Scenario: Test Mobile user able to remove the item in the cart to view Empty cart page
	And I remove the item from the cart
	Then I see Empty Cart page

Scenario: Test Mobile user able to decrease the item in the cart
	And I increase the count of the item
	And I decrease the count of the item
	Then I see SubTotal Value gets updated

Scenario: Test Mobile user able to add promo code to get discounts
	And I apply valid promocode PROMOCODE1
	Then I see the discount added to cart